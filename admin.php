<?php

if (empty($_SERVER['PHP_AUTH_USER']) ||
    empty($_SERVER['PHP_AUTH_PW']) ||
    $_SERVER['PHP_AUTH_USER'] != 'admin' ||
    md5($_SERVER['PHP_AUTH_PW']) != md5('123')) {
    header('HTTP/1.1 401 Unauthorized');
    header('WWW-Authenticate: Basic realm="My site"');
    print('<h1>Авторизуйтесь</h1>');
    exit();
}

$db = new PDO('mysql:host=localhost;dbname=u16432', 'u16432', '2294342', array(
    PDO::ATTR_PERSISTENT => true
));

$token = md5('mytoken' . $_SERVER['PHP_AUTH_USER']);
setcookie('token', $token, time() + 60 * 60 * 2);

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
 if ($POST['token'] === $_COOKIE['token']) {
    try {
        $stmt = $db->prepare('DELETE FROM wb5 WHERE login = ?');
        $stmt->execute(array(
          strip_tags($_POST['delete'])
        ));

    } catch (PDOException $e) {
        echo 'Ошибка: ' . $e->getMessage();
        exit();
    }
}
    header('Location: ./admin.php');
}

try {
    $stmt = $db->query('SELECT * FROM wb5');
    ?>
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>WB6 Admin</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>
    <body>
    <form action="" method="post">
	<input type="hidden" name="token" value="<?php print($token)?>">
        <table class="table table-striped table-info table-hover">
            <thead>
            <tr>
                <th>id</th>
                <th>Логин</th>
                <th>Пароль</th>
                <th>Имя</th>
                <th>E-mail</th>
                <th>Год</th>
                <th>Пол</th>
                <th>Конечности</th>
                <th>Способности</th>
                <th>Биография</th>
                <th>Удалить</th>
            </tr>
            </thead>
            <tbody>
            <?php
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)) {
                print('<tr>');
                foreach ($row as $cell) {
                    print('<td>' . $cell . '</td>');
                }
                print('<td><button class="btn" name="delete" type="submit" value="' . strip_tags($row['login']) . '">Х</button></td>');
                print('</tr>');
            }
            ?>
            </tbody>
        </table>
        <a href="../../wb6/" class="btn btn-primary btn-success btn-lg active" role="button" aria-pressed="true">Вернуться к форме</a>
    </form>
    </body>
    <?php
} catch (PDOException $e) {
    echo 'Ошибка: ' . $e->getMessage();
    exit();
}