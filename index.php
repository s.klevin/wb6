<?php
header('Content-Type: text/html; charset=UTF-8');

$db_user = 'u16432'; //логин БД
$db_pass = '2294342'; //пароль БД
$db = new PDO('mysql:host=localhost;dbname=u16432', $db_user, $db_pass, array(
  PDO::ATTR_PERSISTENT => true
));

if ($_SERVER['REQUEST_METHOD'] == 'GET') {
  $messages = array();		//массив для временного хранения сообщений
  $messages['save'] = '';
  $messages['notsave'] = '';
  $messages['name'] = '';
  $messages['email'] = '';
  $messages['powers'] = '';
  $messages['bio'] = '';
  $messages['check'] = '';

  if (!empty($_COOKIE['save'])) {
    setcookie('save', '', 100000);	//удаляем куку
    setcookie('login', '', 100000);
    setcookie('pass', '', 100000);

    $messages['save'] = 'Результаты успешно сохранен.';	    //сообщение пользователю

  if (!empty($_COOKIE['pass'])) {
      $messages['savelogin'] = sprintf(' Вы можете <a href="login.php">войти</a> с логином <strong>%s</strong>
        и паролем <strong>%s</strong> для изменения данных.',
        strip_tags($_COOKIE['login']),
        strip_tags($_COOKIE['pass']));
    }
  }

  if (!empty($_COOKIE['notsave'])) {
    setcookie('notsave', '', 100000);
    $messages['notsave'] = strip_tags($_COOKIE['notsave']);
  }

  $errors = array();
  $errors['name'] = empty($_COOKIE['name_error']) ? '' : $_COOKIE['name_error'];
  $errors['email'] = empty($_COOKIE['email_error']) ? '' : $_COOKIE['email_error'];
  $errors['powers'] = empty($_COOKIE['powers_error']) ? '' : $_COOKIE['powers_error'];
  $errors['bio'] = empty($_COOKIE['bio_error']) ? '' : $_COOKIE['bio_error'];
  $errors['check'] = empty($_COOKIE['check_error']) ? '' : $_COOKIE['check_error'];

  //проверка на ошибки
  if ($errors['name'] == 'null') {
    setcookie('name_error', '', 100000);
    $messages['name'] = 'Введите имя. ';
  }
  else if ($errors['name'] == 'incorrect') {
      setcookie('name_error', '', 100000);
      $messages['name'] = 'Имя введено неверно. ';
  }

  if ($errors['email']) {
    setcookie('email_error', '', 100000);
    $messages['email'] = 'Введите почту. ';
  }

  if ($errors['powers']) {
    setcookie('powers_error', '', 100000);
    $messages['powers'] = 'Выберите сверхспособность.';
  }

  if ($errors['bio']) {
    setcookie('bio_error', '', 100000);
    $messages['bio'] = 'Введите биографию. ';
  }

  if ($errors['check']) {
    setcookie('check_error', '', 100000);
    $messages['check'] = 'Вы не согласны с контрактом, исправьте это. ';
  }

  $values = array();
  $powers = array();

  $powers['levit'] = "Левитация";
  $powers['tp'] = "Телепортация";
  $powers['walk'] = "Хождение сквозь стены";

  $values['name'] = empty($_COOKIE['name_value']) ? '' : strip_tags($_COOKIE['name_value']);
  $values['email'] = empty($_COOKIE['email_value']) ? '' : strip_tags($_COOKIE['email_value']);
  $values['age'] = empty($_COOKIE['age_value']) ? '' : strip_tags($_COOKIE['age_value']);
  $values['sex'] = empty($_COOKIE['sex_value']) ? 'male' : strip_tags($_COOKIE['sex_value']);
  $values['limbs'] = empty($_COOKIE['limbs_value']) ? '4' : strip_tags($_COOKIE['limbs_value']);
  $values['bio'] = empty($_COOKIE['bio_value']) ? '' : strip_tags($_COOKIE['bio_value']);
  $powers_value = empty($_COOKIE['powers_value']) ? '' : json_decode($_COOKIE['powers_value']);

  $values['powers'] = [];
  if (isset($powers_value) && is_array($powers_value)) {
      foreach ($powers_value as $power) {
          if (!empty($powers[$power])) {
              $values['powers'][$power] = $power;
          }
      }
  }

if (!empty($_COOKIE[session_name()]) && session_start() && !empty($_SESSION['login'])) {
    $messages['save'] = ' ';
    $messages['savelogin'] = 'Вход с логином '.$_SESSION['login'];

    try {
      $stmt = $db->prepare("SELECT * FROM backend5 WHERE uid = ?");
      $stmt->execute(array(
        $_SESSION['login']
      ));
      $user_data = $stmt->fetch();

      $values['name'] = strip_tags($user_data['name']);
      $values['email'] = strip_tags($user_data['email']);
      $values['age'] = strip_tags($user_data['age']);
      $values['sex'] = strip_tags($user_data['sex']);
      $values['limbs'] = strip_tags($user_data['limbs']);
      $values['bio'] = strip_tags($user_data['bio']);
      $powers_value = explode(", ", $user_data['powers']);
      $values['powers'] = [];
      foreach ($powers_value as $power) {
        if (!empty($powers[$power])) {
          $values['powers'][$power] = $power;
        }
      }
    }
    catch(PDOException $e) {
      echo "Ошибка загрузки данных из БД.";
      exit();
    }
  }
  include('form.php'); //подключаем form.php.
}

else {
  $errors = FALSE;	//проверяем ошибки
  if (empty($_POST['name'])) {
    setcookie('name_error', 'null', time() + 24 * 60 * 60);	//даём куку на сутки
    $errors = TRUE;
  }
  else if (!preg_match("#^[aA-zZ0-9-]+$#", $_POST["name"])) {
      setcookie('name_error', 'incorrect', time() + 24 * 60 * 60);
      $errors = TRUE;
  }
  else {
    setcookie('name_value', $_POST['name'], time() + 7 * 24 * 60 * 60);	//сохраняем на неделю
  }

  if (empty($_POST['email'])) {
    setcookie('email_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('email_value', $_POST['email'], time() + 7 * 24 * 60 * 60);
  }

  $powers = array();

  foreach ($_POST['powers'] as $key => $value) {
      $powers[$key] = $value;
  }

  if (!sizeof($powers)) {
    setcookie('powers_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('powers_value', json_encode($powers), time() + 7 * 24 * 60 * 60);
  }

  if (empty($_POST['bio'])) {
    setcookie('bio_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }
  else {
    setcookie('bio_value', $_POST['bio'], time() + 7 * 24 * 60 * 60);
  }

  if (empty($_POST['check'])) {
    setcookie('check_error', '1', time() + 24 * 60 * 60);
    $errors = TRUE;
  }

  setcookie('age_value', $_POST['age'], time() + 7 * 24 * 60 * 60);
  setcookie('sex_value', $_POST['sex'], time() + 7 * 24 * 60 * 60);
  setcookie('limbs_value', $_POST['limbs'], time() + 7 * 24 * 60 * 60);

  if ($errors) {
    header('Location: index.php');
    exit();
  }
  else {	//удаляем куки с ошибками
    setcookie('name_error', '', 100000);
    setcookie('name_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('powers_error', '', 100000);
    setcookie('bio_error', '', 100000);
    setcookie('check_error', '', 100000);
  }

  if (!empty($_COOKIE[session_name()]) &&
      session_start() && !empty($_SESSION['login'])) {

    try {
      $stmt = $db->prepare("UPDATE wb5 SET name = ?, email = ?, age = ?, sex = ?, limbs = ?, powers = ?, bio = ? WHERE uid = ?");
      $stmt->execute(array(
        $_POST['name'],
        $_POST['email'],
        $_POST['age'],
        $_POST['sex'],
        $_POST['limbs'],
        implode(', ', $_POST['powers']),
        $_POST['bio'],
        $_SESSION['login']
      ));
    }
    catch(PDOException $e) {
      setcookie('notsave', 'Ошибка: ' . $e->getMessage());
      exit();
    }
  }
  else {
    $login = uniqid("id");
    $pass = rand(11111, 99999);
    setcookie('login', $login);
    setcookie('pass', $pass);

try {

      $stmt_form = $db->prepare("INSERT INTO backend5 SET login = ?, pass = ?, name = ?, email = ?, age = ?, sex = ?, limbs = ?, powers = ?, bio = ?");
      $stmt_form->execute(array(
        $login,
        hash('md5', $pass, false),
        $_POST['name'],
        $_POST['email'],
        $_POST['age'],
        $_POST['sex'],
        $_POST['limbs'],
        implode(', ', $_POST['powers']),
        $_POST['bio']
      ));
}
    catch(PDOException $e) {
      // При возникновении ошибки отправления в БД, выводим информацию
      setcookie('notsave', 'Ошибка: ' . $e->getMessage());
      exit();
    }
  }

  setcookie('save', '1');	  //сохраняем куку

  header('Location: ./');
}

