<html lang="ru">
  <head>
      <title>WB6</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <link href="style.css" type="text/css" rel="stylesheet">
  </head>
  <body>
	<a href="admin.php" class="btn">Получить права администратора</a>

    <form action="" method="POST">
        <?php
        if (!empty($messages)) 
        {
          print('<div id="messages">');
          foreach ($messages as $message){
            print($message);
          }
          print('</div>');
        }
        ?>

      <label for="nameInput">Имя </label>
      <input id="nameInput" name="name" type="text" <?php if ($errors['name']) {print 'class="error"';} ?> value="<?php print $values['name']; ?>" />

      <label for="emailInput">E-mail </label>
      <input id="emailInput" name="email" type="email" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" />

      <label for="selectInput">Год рождения</label>
      <select name="age">
          <?php
            for ($i = 2002; $i > 1950; $i--) {
              print('<option value="'.$i.'" ');
              if ($values['age'] == $i) print('selected ');
              print('>'.$i.'</option> ');
            }
           ?>
      </select>

      <label>Пол</label>
      <label>
          <input type="radio" name="sex" value="male" <?php if ($values['sex'] == 'male') print("checked"); ?> >
           Мужской
      </label>
      <label>
          <input type="radio" name="sex" value="female" <?php if ($values['sex'] == 'female') print("checked"); ?> >
           Женский
      </label>

      <label>Количество конечностей</label>
      <label>
          <input type="radio" name="limbs" value="2" <?php if ($values['limbs'] == 2) print("checked"); ?> >
           2
      </label>
      <label>
          <input type="radio" name="limbs" value="4" <?php if ($values['limbs'] == 4) print("checked"); ?> >
           4
      </label>
      <label>
          <input type="radio" name="limbs" value="8" <?php if ($values['limbs'] == 8) print("checked"); ?> >
           8
      </label>

      <label for="powersSelect">Суперспособности</label>
      <select id="powersSelect" <?php if ($errors['powers']) {print 'class="error"';} ?> name="powers[]" multiple size="3">
         <?php
            foreach ($powers as $key => $value) {
                $selected = empty($values['powers'][$key]) ? '' : ' selected="selected" ';
                printf('<option value="%s",%s>%s</option>', $key, $selected, $value);
            }
         ?>
      </select>

      <label for="bioArea">Биография</label>
      <textarea id="bioArea" name="bio" rows="8" cols="30" placeholder="Расскажите о себе" <?php if ($errors['bio']) {print 'class="error"';} ?>><?php print $values['bio']; ?></textarea>

      <label <?php if ($errors['check']) {print 'class="error"';} ?>><input type="checkbox" name="check" value="ok"> С контрактом ознакомлен(-а)</label>

      <input type="submit" value="Отправить" />
    </form>
  </body>
</html>
